using businesslayer;
using businesslayer.Implementations;
using businesslayer.Interfaces;
using datalayer;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
string conString = builder.Configuration["ConnectionStrings:DefaultConnection"];
builder.Services.AddDbContext<EFDBContext>(opt => opt.UseSqlServer(conString, b=>b.MigrationsAssembly("datalayer")));
builder.Services.AddTransient<IDirectoryRepo, EFDirectoryRepo>();
builder.Services.AddTransient<IMaterialRepo, EFMaterialRepo>();
builder.Services.AddScoped<DataManager>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<EFDBContext>();
    SampleData.InitData(context);
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
