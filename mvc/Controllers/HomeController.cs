﻿using System.Diagnostics;
using datalayer;
using Microsoft.AspNetCore.Mvc;
using mvc.Models;
using System.Linq;
using Directory = datalayer.Entities.Directory;


using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using businesslayer.Interfaces;
using businesslayer.Implementations;
using businesslayer;

namespace mvc.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private EFDBContext _context;

    private IDirectoryRepo _directoryRepo;
    private DataManager _dataManager;
    public HomeController(ILogger<HomeController> logger, EFDBContext context, IDirectoryRepo directoryRepo, DataManager dataManager)
    {
        _context = context;
        _logger = logger;
        _directoryRepo = directoryRepo;
        _dataManager = dataManager;
    }

    public IActionResult Index()
    {
        HelloModel _model = new HelloModel() {HelloMessage = "Hello, Dkey!"};
        // List<Directory> _dirs = _context.Directory.Include(x=>x.Materials).ToList();
        // List<Directory> _dirs = _directoryRepo.GetAllDirectories(true).ToList();
        List<Directory> _dirs = _dataManager.Directories.GetAllDirectories(true).ToList();
        return View(_dirs);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
