using datalayer.Entities;
using Directory = datalayer.Entities.Directory;

namespace datalayer.Entities;

public class Material : Page
{
    public int DirectoryId { get; set; }
    public Directory? Directory { get; set; }
}