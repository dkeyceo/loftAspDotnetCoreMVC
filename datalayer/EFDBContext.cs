using datalayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Directory = datalayer.Entities.Directory;

namespace datalayer;
public class EFDBContext : DbContext
{
    public DbSet<Directory>? Directory { get; set; }
    public DbSet<Material>? Material { get; set; }

    public EFDBContext(DbContextOptions<EFDBContext> options) : base(options) { }
}

public class EFDBContextFactory : IDesignTimeDbContextFactory<EFDBContext>
{
    public EFDBContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<EFDBContext>();
        optionsBuilder.UseSqlServer("Data Source=localhost\\sqlexpress; Integrated Security=True; Database=loftblog");
        return new EFDBContext(optionsBuilder.Options);
    }
}
