using datalayer;
using datalayer.Entities;
using System.Linq;
using Directory = datalayer.Entities.Directory;

namespace datalayer;

public static class SampleData
{
    public static void InitData(EFDBContext context)
    {
        if(!context.Directory.Any()){

            context.Directory.Add(new Entities.Directory() {Title="First Directory", Html="<b>Directory content 1</b>"});
            context.Directory.Add(new Entities.Directory() {Title="Second Directory", Html="<b>Directory content 2</b>"});
            context.Directory.Add(new Entities.Directory() {Title="Third Directory", Html="<b>Directory content 3</b>"});
            context.SaveChanges();

            context.Material.Add(new Entities.Material() {Title="First Material", Html="<i>Material content 1</i>", DirectoryId = 7});
            context.Material.Add(new Entities.Material() {Title="Second Material", Html="<i>Material content 2</i>", DirectoryId = 8});
//            context.Material.Add(new Entities.Material() {Title="Third Material", Html="<i>Material content 3</i>", DirectoryId = context.Directory.First().Id});
            context.SaveChanges();

        }
    }
}