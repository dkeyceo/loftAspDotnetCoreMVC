using businesslayer.Interfaces;

namespace businesslayer;

public class DataManager 
{
    private IDirectoryRepo _directoryRepo;
    private IMaterialRepo _materialRepo;
    public DataManager(IDirectoryRepo directoryRepo, IMaterialRepo materialRepo)
    {
        _directoryRepo = directoryRepo;
        _materialRepo = materialRepo;
    }
    public IDirectoryRepo ?Directories {get {return _directoryRepo;} }
    public IMaterialRepo ?Materials {get {return _materialRepo;} }
}