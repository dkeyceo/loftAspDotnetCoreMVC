using System.Linq;
using businesslayer.Interfaces;
using datalayer;
using Directory = datalayer.Entities.Directory;

using System.Diagnostics;


using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace businesslayer.Implementations;

public class EFDirectoryRepo : IDirectoryRepo
{
    private EFDBContext _context;
    public EFDirectoryRepo (EFDBContext context)
    {
        _context = context;
    }
    public void DeleteDirectory(datalayer.Entities.Directory directory)
    {
        _context.Directory.Remove(directory);
        _context.SaveChanges();
    }

    public IEnumerable<datalayer.Entities.Directory> GetAllDirectories(bool includeMaterials = false)
    {
        if(includeMaterials)
            return _context.Set<Directory>().Include(x=>x.Materials).AsNoTracking().ToList();
        else
            return _context.Directory.ToList();
    }

    public datalayer.Entities.Directory GetDirectoryById(int directoryId, bool includeMaterials = false)
    {
        if(includeMaterials)
            return _context.Set<Directory>().Include(x=>x.Materials).AsNoTracking().FirstOrDefault(x=>x.Id == directoryId);
        else
            return _context.Directory.FirstOrDefault(x => x.Id == directoryId);
    }

    public void SaveDirectory(datalayer.Entities.Directory directory)
    {
        if(directory.Id == 0)
            _context.Directory.Add(directory);
        else
            _context.Entry(directory).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        
        _context.SaveChanges();
    }
}