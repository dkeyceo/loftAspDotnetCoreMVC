using businesslayer.Interfaces;
using datalayer;
using datalayer.Entities;
using Directory = datalayer.Entities.Directory;

using System.Linq;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace businesslayer.Implementations;

public class EFMaterialRepo : IMaterialRepo
{
    private EFDBContext _context;
    public EFMaterialRepo (EFDBContext context)
    {
        _context = context;
    }
    public void DeleteDirectory(Material material)
    {
        _context.Material.Remove(material);
        _context.SaveChanges();
    }

    public IEnumerable<Material> GetAllMaterials(bool includeDirectory = false)
    {
         if(includeDirectory)
            return _context.Set<Material>().Include(x=>x.Directory).AsNoTracking().ToList();
        else
            return _context.Material.ToList();
    }

    public Material GetMaterialById(int materialId, bool includeDirectory = false)
    {
        if(includeDirectory)
            return _context.Set<Material>().Include(x=>x.Directory).AsNoTracking().FirstOrDefault(x=>x.Id == materialId);
        else
            return _context.Material.FirstOrDefault(x => x.Id == materialId);
    }

    public void SaveDirectory(Material material)
    {
        if(material.Id == 0)
            _context.Material.Add(material);
        else
            _context.Entry(material).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        
        _context.SaveChanges();
    }
}