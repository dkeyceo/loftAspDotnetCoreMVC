using datalayer.Entities;

namespace businesslayer.Interfaces;

public interface IMaterialRepo
{
    IEnumerable<Material> GetAllMaterials (bool includeDirectory = false);
    Material GetMaterialById(int directoryId, bool includeDirectory = false);
    void SaveDirectory(Material material);
    void DeleteDirectory(Material material);
}
