using Directory = datalayer.Entities.Directory;
using System.Collections.Generic;
namespace businesslayer.Interfaces;

public interface IDirectoryRepo
{
    IEnumerable<Directory> GetAllDirectories (bool includeMaterials = false);
    Directory GetDirectoryById(int directoryId, bool includeMaterials = false);
    void SaveDirectory(Directory directory);
    void DeleteDirectory(Directory directory);
}